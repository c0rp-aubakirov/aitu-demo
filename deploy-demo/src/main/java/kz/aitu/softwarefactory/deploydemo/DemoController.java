package kz.aitu.softwarefactory.deploydemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Sanzhar Aubakirov (c0rp.aubakirov@gmail.com)
 */
@RestController
@RequestMapping("/aitu-demo")
public class DemoController {

    @GetMapping("/demo-get")
    public String demoGet() {
        return "{\"name\":\"Sanzhar\"}";
    }

    @PostMapping("/demo-post")
    public String demoPost() {
        return "{\"name\":\"Sanzhar POST\"}";
    }
}
